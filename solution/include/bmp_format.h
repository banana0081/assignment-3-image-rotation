#ifndef BMP_F
#define BMP_F
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define type 19778
#define bitCount 24
#define paramDefault 0
#define planes 1
#define info_size 40
#define header_size 54

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_FILE_ERROR
  /* коды других ошибок  */
  };

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};
#pragma pack(pop)
struct bmp_header create_header(uint64_t width, uint64_t height);

enum read_status validate_header(const struct bmp_header* const header);
#endif
