#ifndef BMPIO
#define BMPIO
#include "bmp_format.h"
#include <stdio.h>

enum read_status from_bmp( FILE* in, struct image* image );

enum write_status to_bmp(FILE* out, struct image const* image);



#endif
