#include "../include/bmp_format.h"
#include "../include/bmp_io.h"


struct bmp_header create_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    header.bfType = type;
    header.bfileSize = (uint32_t)sizeof(struct bmp_header) + (uint32_t)(sizeof(struct pixel)*width + (4 - (width * sizeof(struct pixel))) % 4)*(uint32_t)height;
    header.bfReserved = paramDefault;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = info_size;
    header.biWidth = (uint32_t)width;
    header.biHeight = (uint32_t)height;
    header.biPlanes = planes;
    header.biBitCount = bitCount;
    header.biCompression = paramDefault;
    header.biSizeImage = (uint32_t)(sizeof(struct pixel)*width + (4 - (width * sizeof(struct pixel))) % 4)*(uint32_t)height;
    header.biXPelsPerMeter = paramDefault;
    header.biYPelsPerMeter = paramDefault;
    header.biClrUsed = paramDefault;
    header.biClrImportant = paramDefault;
    return header;

}

enum read_status validate_header(const struct bmp_header* const header) {
    if (header->bfType != type) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->bfReserved != paramDefault ||
        header->biPlanes != planes ||
        header->biBitCount != bitCount ||
        header->bOffBits < sizeof (struct bmp_header))
    {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}



