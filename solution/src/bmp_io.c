#include "../include/bmp_io.h"
static inline uint8_t calculate_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel))) % 4;
}
enum read_status from_bmp( FILE* in, struct image* image ){
    if (in == NULL || image == NULL){return READ_INVALID_BITS;}
    struct bmp_header header;
    if (fread(&header, 54, 1, in) != 1){return READ_INVALID_HEADER;}
    if(validate_header(&header)!=READ_OK){
        return validate_header(&header);
    }
    *image = create_image((uint64_t)header.biWidth, header.biHeight);
    if(image->width*image->height==0){
        destroy_image(image);
        return READ_INVALID_HEADER;
    }
    for (uint32_t y=0; y<image->height; y++){
        if(fread(get_pixel(0, y, image), sizeof(struct pixel), image->width, in) != image->width || (fseek(in, calculate_padding(image->width), SEEK_CUR) != 0)){
            destroy_image(image);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;

}   

enum write_status to_bmp( FILE* out, struct image const* image ){
    uint64_t width = image -> width;
    uint64_t height = image -> height;

    size_t padding = calculate_padding(image->width);

    struct bmp_header header = create_header(width, height);
    
    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) return WRITE_ERROR;

    size_t buf = 0;
    size_t img_width = image->width;
    for (uint32_t i = 0; i < image->height; i++){
        size_t width_2 = fwrite(image->data + i * image->width, sizeof(struct pixel), img_width, out);
        if (width_2 != img_width) return WRITE_ERROR;
        if(!fwrite(&buf, padding, 1, out)) return WRITE_ERROR;
    }
    return WRITE_OK;

}
