#include "image.h"

struct pixel *get_pixel(uint64_t x, uint64_t y, struct image const *image) {
    return image->data + x + y * image->width;
}


struct image create_image(uint64_t width, uint64_t height) {
    void *data = malloc(width * height * sizeof(struct pixel));
    if (data == NULL) {
      return (struct image) {0};
    }
    return (struct image) {
            width, height, data
    };
}

void destroy_image(struct image *image) {
    if (image != NULL) {
        free(image->data);
        image->width = 0;
        image->height = 0;
        image->data = NULL;
    }
}
