#include <bmp_io.h>
#include <image_transformer.h>

int main(int argc, char **argv) {

    FILE *input = NULL;
    FILE *output = NULL;
    if (argc != 3) {
        fprintf(stderr, "Application takes exactly 2 parameters.\n");
        if(argc == 1){
            input = fopen("input.bmp", "rb");
            output = fopen("output.bmp", "wb");
        }
    }
    else{
        input = fopen(argv[1], "rb");
        output = fopen(argv[2], "wb");
    }

    
    
    if (input == NULL) {
        fprintf(stderr, "Source file could not be opened \n");
        if (fclose(input) == EOF) {
            fprintf(stderr, "Error while closing input file \n");
        }
        return -1;
        
    }


    if (output == NULL) {
        fprintf(stderr, "Output file could not be opened \n");
        if (fclose(input) == EOF) {
            fprintf(stderr, "Error while closing input file \n");
        }
        return -1;
    }

    struct image image = {0};

    enum read_status read_status = from_bmp(input, &image);
    if (fclose(input) == EOF) {
            fprintf(stderr, "Error while closing input file \n");
    }
     if (read_status == READ_OK) {
        struct image rotated = rotate(&image);
        destroy_image(&image);
        fprintf(stdout, "Successfully rotated! \n");
        enum write_status write_status = to_bmp(output, &rotated);
        if (write_status == WRITE_OK) {
            fprintf(stdout, "Successfully finished! \n");
            destroy_image(&rotated);
            if (fclose(output) == EOF) {
            fprintf(stderr, "Error while closing output file \n");
            }
        } else if (write_status == WRITE_ERROR) {
            fprintf(stderr, "Error while writing output \n");
            destroy_image(&rotated);
            if (fclose(output) == EOF) {
            fprintf(stderr, "Error while closing output file \n");
            }
            return -1;
        }
        return 0;
    } else {
        if (read_status == READ_INVALID_HEADER) {
            fprintf(stderr, "Wrong header in source \n");
        } else if (read_status == READ_INVALID_SIGNATURE) {
            fprintf(stderr, "Wrong signature in source \n");
        } else if (read_status == READ_INVALID_BITS) {
            fprintf(stderr, "Incorrect bits in source \n");
        }
        destroy_image(&image);
        return -1;
    }
}
